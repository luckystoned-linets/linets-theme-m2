define('jquery', function($) {

    // Minicart up-down product count
    $.fn.LinetsMinicartQty = function() {
        cartWrapper = $('#minicart-content-wrapper');
        cartWrapper.on('click', 'span', function() {
            
            qtyField = $(this).parent().find('.item-qty');
            qtyBtn = $(this).hasClass('up-qty');
            qtyOrgVal = qtyField.attr('data-item-qty');
            update = $(this).parent().find('.update-cart-item');
            qtyState = parseInt(qtyField.val());

            if (qtyBtn) {
                result = qtyField.val(qtyState +1);
                update.show();
            }else if (qtyState > 0 && !qtyBtn) {
                result = qtyField.val(qtyState -1);
                update.show();
            }

            qtyStateResult = parseInt(qtyField.val());
            if(qtyOrgVal == qtyStateResult){
                update.hide();
            }
            
        });
    }

    // wishlist split category z-index menu active
    $.fn.LinetsWishlistCatMenu = function() {

        $(document).on('buildWishlistDropdown', function() { 
            var toggleWishlist = $('.actions-secondary .split .action.toggle');

            toggleWishlist.on('click',function(e) {
                var productParent = $(this).closest('.product-item-info');

                if(!$(this).hasClass('active')){
                    productParent.css("z-index", "10");

                    $(productParent).mouseleave(function() {

                        productParent.removeAttr("style");
                        $('.item.product.product-item').removeClass('leftWishlist');
                    });
                }

                if($(this).hasClass('active')){

                    productParent.removeAttr("style");
                    $('.item.product.product-item').removeClass('leftWishlist');
                }
                
                // calculate how many boxes will be in a "row" 
                var windowWidth = $('.list.items.product-items').width();
                var boxWidth = $('.item.product.product-item').outerWidth();
                var boxesPerRow = ~~(windowWidth / boxWidth);
                var index = productParent.parent().index();

                // get the column of the clicked element
                var col = (index % boxesPerRow) + 1;

                // calculate how far it is to the end of this row, 
                // and select that element
                var $endOfRow = $('.item.product.product-item').eq(index + boxesPerRow - col);

                if (!$endOfRow.length) $endOfRow = $('.item.product.product-item').last();
                
                $endOfRow.addClass('leftWishlist');
            });
        });

    }

}(jQuery));
